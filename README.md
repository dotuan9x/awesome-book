## Tổng hợp những cuốn [sách nói hay](https://nghesachnoi.com/) nên đọc trong đời.

### Sách kỹ năng
1. [Đắc Nhân Tâm – Dale Carnegie](https://nghesachnoi.com/book/dac-nhan-tam--dale-carnegie-p4.html)
2. [Nghĩ Đơn Giản, Sống Đơn Thuần](https://nghesachnoi.com/book/nghi-don-gian-song-don-thuan-p32.html)
3. [48 Nguyên Tắc Chủ Chốt Của Quyền Lực](https://nghesachnoi.com/book/48-nguyen-tac-chu-chot-cua-quyen-luc-p1.html)
4. [Bốn Mươi Gương Thành Công - Dale Carnegie](https://nghesachnoi.com/book/bon-muoi-guong-thanh-cong-p31.html)
5. [Muôn Kiếp Nhân Sinh - Nguyên Phong](https://nghesachnoi.com/book/muon-kiep-nhan-sinh-nguyen-phong-p606.html)
6. [Muôn Kiếp Nhân Sinh 2 - Nguyên Phong](https://nghesachnoi.com/book/muon-kiep-nhan-sinh-2-nguyen-phong-p607.html)
7. [Minh Triết Trong Đời Sống - Nguyên Phong](https://nghesachnoi.com/book/minh-triet-trong-doi-song-p704.html)
8. [Bên Rặng Tuyết Sơn - Nguyên Phong](https://nghesachnoi.com/book/ben-rang-tuyet-son-p666.html)
9. [Đời Ngắn Đừng Ngủ Dài](https://nghesachnoi.com/book/doi-ngan-dung-ngu-dai-p418.html)
10. [Hoa Sen Trên Tuyết - Nguyên Phong](https://nghesachnoi.com/book/hoa-sen-tren-tuyet-p616.html)
11. [Trở Về Từ Xứ Tuyết - Nguyên Phong](https://nghesachnoi.com/book/tro-ve-tu-xu-tuyet-nguyen-phong-p609.html)
12. [Dấu Chân Trên Cát - Nguyên Phong](https://nghesachnoi.com/book/dau-chan-tren-cat-nguyen-phong-p613.html)
13. [Hành Trình Về Phương Đông - Nguyên Phong](https://nghesachnoi.com/book/hanh-trinh-ve-phuong-dong-p614.html)
14. [Trở Về Từ Cõi Sáng - Nguyên Phong](https://nghesachnoi.com/book/tro-ve-tu-coi-sang-nguyen-phong-p610.html)

### Sách kỹ thuật

